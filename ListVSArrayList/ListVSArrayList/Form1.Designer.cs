﻿namespace ListVSArrayList
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelAddInt = new System.Windows.Forms.Label();
            this.labelAddString = new System.Windows.Forms.Label();
            this.labelGetInt = new System.Windows.Forms.Label();
            this.labelGetString = new System.Windows.Forms.Label();
            this.labelSortInt = new System.Windows.Forms.Label();
            this.labelSortString = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelSortObjString = new System.Windows.Forms.Label();
            this.labelSortObjInt = new System.Windows.Forms.Label();
            this.labelGetObjString = new System.Windows.Forms.Label();
            this.labelGetObjInt = new System.Windows.Forms.Label();
            this.labelAddObjString = new System.Windows.Forms.Label();
            this.labelAddObjInt = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(26, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(541, 60);
            this.label1.TabIndex = 0;
            this.label1.Text = "Оценить скорость а) добавления элементов b) получения элементов\r\n с) сортировки э" +
    "лементов в коллекциях типа List<> и ArrayList. \r\nИспользовать элементы типа Syst" +
    "em.Int32 и System.String.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(26, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "List";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(632, 501);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 30);
            this.button1.TabIndex = 2;
            this.button1.Text = "Оценить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(26, 403);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "ArrayList";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(213, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Добавление элемента";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(121, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = " System.Int32 ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(121, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = " System.String";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(375, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Получение элемента";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(542, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = " Сортировка элементов ";
            // 
            // labelAddInt
            // 
            this.labelAddInt.AutoSize = true;
            this.labelAddInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddInt.Location = new System.Drawing.Point(243, 201);
            this.labelAddInt.Name = "labelAddInt";
            this.labelAddInt.Size = new System.Drawing.Size(0, 16);
            this.labelAddInt.TabIndex = 9;
            // 
            // labelAddString
            // 
            this.labelAddString.AutoSize = true;
            this.labelAddString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddString.Location = new System.Drawing.Point(243, 361);
            this.labelAddString.Name = "labelAddString";
            this.labelAddString.Size = new System.Drawing.Size(0, 16);
            this.labelAddString.TabIndex = 10;
            // 
            // labelGetInt
            // 
            this.labelGetInt.AutoSize = true;
            this.labelGetInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelGetInt.Location = new System.Drawing.Point(406, 201);
            this.labelGetInt.Name = "labelGetInt";
            this.labelGetInt.Size = new System.Drawing.Size(0, 16);
            this.labelGetInt.TabIndex = 11;
            // 
            // labelGetString
            // 
            this.labelGetString.AutoSize = true;
            this.labelGetString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelGetString.Location = new System.Drawing.Point(406, 361);
            this.labelGetString.Name = "labelGetString";
            this.labelGetString.Size = new System.Drawing.Size(0, 16);
            this.labelGetString.TabIndex = 12;
            // 
            // labelSortInt
            // 
            this.labelSortInt.AutoSize = true;
            this.labelSortInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSortInt.Location = new System.Drawing.Point(582, 201);
            this.labelSortInt.Name = "labelSortInt";
            this.labelSortInt.Size = new System.Drawing.Size(0, 16);
            this.labelSortInt.TabIndex = 13;
            // 
            // labelSortString
            // 
            this.labelSortString.AutoSize = true;
            this.labelSortString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSortString.Location = new System.Drawing.Point(582, 361);
            this.labelSortString.Name = "labelSortString";
            this.labelSortString.Size = new System.Drawing.Size(0, 16);
            this.labelSortString.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(74, 515);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(359, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "Значения указаны в секундах на миллион элементов";
            // 
            // labelSortObjString
            // 
            this.labelSortObjString.AutoSize = true;
            this.labelSortObjString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSortObjString.Location = new System.Drawing.Point(582, 408);
            this.labelSortObjString.Name = "labelSortObjString";
            this.labelSortObjString.Size = new System.Drawing.Size(0, 16);
            this.labelSortObjString.TabIndex = 21;
            // 
            // labelSortObjInt
            // 
            this.labelSortObjInt.AutoSize = true;
            this.labelSortObjInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSortObjInt.Location = new System.Drawing.Point(582, 248);
            this.labelSortObjInt.Name = "labelSortObjInt";
            this.labelSortObjInt.Size = new System.Drawing.Size(0, 16);
            this.labelSortObjInt.TabIndex = 20;
            // 
            // labelGetObjString
            // 
            this.labelGetObjString.AutoSize = true;
            this.labelGetObjString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelGetObjString.Location = new System.Drawing.Point(406, 408);
            this.labelGetObjString.Name = "labelGetObjString";
            this.labelGetObjString.Size = new System.Drawing.Size(0, 16);
            this.labelGetObjString.TabIndex = 19;
            // 
            // labelGetObjInt
            // 
            this.labelGetObjInt.AutoSize = true;
            this.labelGetObjInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelGetObjInt.Location = new System.Drawing.Point(406, 248);
            this.labelGetObjInt.Name = "labelGetObjInt";
            this.labelGetObjInt.Size = new System.Drawing.Size(0, 16);
            this.labelGetObjInt.TabIndex = 18;
            // 
            // labelAddObjString
            // 
            this.labelAddObjString.AutoSize = true;
            this.labelAddObjString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddObjString.Location = new System.Drawing.Point(243, 408);
            this.labelAddObjString.Name = "labelAddObjString";
            this.labelAddObjString.Size = new System.Drawing.Size(0, 16);
            this.labelAddObjString.TabIndex = 17;
            // 
            // labelAddObjInt
            // 
            this.labelAddObjInt.AutoSize = true;
            this.labelAddObjInt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddObjInt.Location = new System.Drawing.Point(243, 248);
            this.labelAddObjInt.Name = "labelAddObjInt";
            this.labelAddObjInt.Size = new System.Drawing.Size(0, 16);
            this.labelAddObjInt.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(121, 408);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 16);
            this.label16.TabIndex = 23;
            this.label16.Text = " System.String";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(121, 248);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 16);
            this.label17.TabIndex = 22;
            this.label17.Text = " System.Int32 ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(26, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "ArrayList";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(26, 356);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 20);
            this.label11.TabIndex = 25;
            this.label11.Text = "List";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 570);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.labelSortObjString);
            this.Controls.Add(this.labelSortObjInt);
            this.Controls.Add(this.labelGetObjString);
            this.Controls.Add(this.labelGetObjInt);
            this.Controls.Add(this.labelAddObjString);
            this.Controls.Add(this.labelAddObjInt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelSortString);
            this.Controls.Add(this.labelSortInt);
            this.Controls.Add(this.labelGetString);
            this.Controls.Add(this.labelGetInt);
            this.Controls.Add(this.labelAddString);
            this.Controls.Add(this.labelAddInt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List VS ArrayList";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelAddInt;
        private System.Windows.Forms.Label labelAddString;
        private System.Windows.Forms.Label labelGetInt;
        private System.Windows.Forms.Label labelGetString;
        private System.Windows.Forms.Label labelSortInt;
        private System.Windows.Forms.Label labelSortString;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelSortObjString;
        private System.Windows.Forms.Label labelSortObjInt;
        private System.Windows.Forms.Label labelGetObjString;
        private System.Windows.Forms.Label labelGetObjInt;
        private System.Windows.Forms.Label labelAddObjString;
        private System.Windows.Forms.Label labelAddObjInt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

