﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListVSArrayList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        const int volume = 1000000;
        ArrayPerformanceAnalyzer arPerfAn;
       

        private void button1_Click(object sender,EventArgs e)
        {
           
            #region Оценить производительность для List<string>
            arPerfAn = new ArrayPerformanceAnalyzer(new List<string>());
            labelAddString.Text = arPerfAn.CheckForAdd(volume).ToString();
            labelSortString.Text = arPerfAn.CheckForSort(volume).ToString();
            labelGetString.Text = arPerfAn.CheckForGet(volume).ToString();
            #endregion

            #region Оценить производительность для List<int>
            arPerfAn = new ArrayPerformanceAnalyzer(new List<int>());
            labelAddInt.Text = arPerfAn.CheckForAdd(volume).ToString();
            labelSortInt.Text = arPerfAn.CheckForSort(volume).ToString();
            labelGetInt.Text = arPerfAn.CheckForGet(volume).ToString();
            #endregion

            #region Оценить производительность для ArrayList System.Int
            arPerfAn = new ArrayPerformanceAnalyzer(new ArrayList(),
                                                        ArrayPerformanceAnalyzer.Types.Int);
            labelAddObjInt.Text = arPerfAn.CheckForAdd(volume).ToString();
            labelSortObjInt.Text = arPerfAn.CheckForSort(volume).ToString();
            labelGetObjInt.Text = arPerfAn.CheckForGet(volume).ToString();
            #endregion

            #region Оценить производительность для ArrayList System.String
            arPerfAn = new ArrayPerformanceAnalyzer(new ArrayList(),
                                                        ArrayPerformanceAnalyzer.Types.String);
            labelAddObjString.Text = arPerfAn.CheckForAdd(volume).ToString();
            labelSortObjString.Text = arPerfAn.CheckForSort(volume).ToString();
            labelGetObjString.Text = arPerfAn.CheckForGet(volume).ToString();
            #endregion
            
        }
    }
}
