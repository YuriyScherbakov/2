﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListVSArrayList
{
    class ArrayPerformanceAnalyzer
    {
        IList arr;
        Types myType;

        public enum Types
        {
            String, Int, Double, Float

        };
        public ArrayPerformanceAnalyzer(ArrayList arr,Types t)
        {
            this.arr = arr;
            this.myType = t;
        }
        public ArrayPerformanceAnalyzer(List<int> lst)
        {
            this.arr = lst;
            this.myType = Types.Int;
        }
        public ArrayPerformanceAnalyzer(List<string> lst)
        {
            this.arr = lst;
            this.myType = Types.String;
        }
        ///<summary>
        ///Returns the value of seconds needed to add specified volume of objects to current array
        ///</summary>
        public double CheckForAdd(int volume)
        {            
            arr.Clear();
            Random rand = new Random();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            switch ( myType )
            {
                case Types.String:
                    {
                        for ( int i = 0; i < volume; i++ )
                        {
                            arr.Add(rand.Next(volume).ToString());
                        }
                        break;
                    }
                case Types.Int:
                    {
                        for ( int i = 0; i < volume; i++ )
                        {
                            arr.Add(rand.Next(volume));
                        }
                        break;
                    }
            }

            stopWatch.Stop();
            TimeSpan tspan = stopWatch.Elapsed;
            return tspan.TotalSeconds;
        }
        ///<summary>
        ///Returns the value of seconds needed to sort current array
        ///</summary>
        public double CheckForSort(int volume)
        {
            this.CheckForAdd(volume); //Заполняем коллекцию

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            if ( arr is ArrayList )
            {
                ( arr as ArrayList ).Sort();
            }
            else if ( myType == Types.String )
            {
                ( arr as List<string> ).Sort();
            }

            else if ( myType == Types.Int )
            {
                ( arr as List<int> ).Sort();
            }

            stopWatch.Stop();
            TimeSpan tspan = stopWatch.Elapsed;
            return tspan.TotalSeconds;
        }
        ///<summary>
        ///Returns the value of seconds needed to get all objects from current array
        ///</summary>
        public double CheckForGet(int volume)
        {
            this.CheckForAdd(volume); //Заполняем коллекцию

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for ( int i = 0; i < volume; i++ )
            {
               object a = arr [i];
            }

            stopWatch.Stop();
            TimeSpan tspan = stopWatch.Elapsed;
            return tspan.TotalSeconds;

        }
    }
}
