﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiplicationTable
{
    public partial class mainForm : Form
    {
        int dimention = 10;

        public mainForm()
        {
            InitializeComponent();
        }

        private void buttonClear_Click(object sender,EventArgs e)
        {
            this.richTextBox1.Clear();
        }

        private void buttonFor_Click(object sender,EventArgs e)
        {
            this.richTextBox1.Clear();
            TableDrawer.forUsing(this.richTextBox1, this.dimention);
        }

        private void buttonDoWhile_Click(object sender,EventArgs e)
        {
            this.richTextBox1.Clear();
            TableDrawer.doWhileUsing(this.richTextBox1,this.dimention);
        }

        private void buttonWhile_Click(object sender,EventArgs e)
        {
            this.richTextBox1.Clear();
            TableDrawer.WhileUsing(this.richTextBox1,this.dimention);
        }

        private void buttonForeach_Click(object sender,EventArgs e)
        {
            this.richTextBox1.Clear();
            TableDrawer.ForeachUsing(this.richTextBox1,this.dimention);
        }
    }
}
