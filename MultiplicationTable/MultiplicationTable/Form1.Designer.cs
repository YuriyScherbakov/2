﻿namespace MultiplicationTable
{
    partial class mainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonFor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonForeach = new System.Windows.Forms.Button();
            this.buttonWhile = new System.Windows.Forms.Button();
            this.buttonDoWhile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(452, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(581, 433);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(926, 460);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(107, 31);
            this.buttonClear.TabIndex = 1;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonFor
            // 
            this.buttonFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonFor.Location = new System.Drawing.Point(16, 114);
            this.buttonFor.Name = "buttonFor";
            this.buttonFor.Size = new System.Drawing.Size(86, 28);
            this.buttonFor.TabIndex = 2;
            this.buttonFor.Text = "for";
            this.buttonFor.UseVisualStyleBackColor = true;
            this.buttonFor.Click += new System.EventHandler(this.buttonFor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(399, 80);
            this.label1.TabIndex = 3;
            this.label1.Text = "Вывести на консоль таблицу умножения \r\n(отформатированный квадрат 10х10)\r\n циклам" +
    "и for, do..while, while, foreach. \r\nДиагональ с квадратами чисел выделить цветом" +
    ". ";
            // 
            // buttonForeach
            // 
            this.buttonForeach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonForeach.Location = new System.Drawing.Point(16, 252);
            this.buttonForeach.Name = "buttonForeach";
            this.buttonForeach.Size = new System.Drawing.Size(86, 28);
            this.buttonForeach.TabIndex = 5;
            this.buttonForeach.Text = "foreach";
            this.buttonForeach.UseVisualStyleBackColor = true;
            this.buttonForeach.Click += new System.EventHandler(this.buttonForeach_Click);
            // 
            // buttonWhile
            // 
            this.buttonWhile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonWhile.Location = new System.Drawing.Point(16, 205);
            this.buttonWhile.Name = "buttonWhile";
            this.buttonWhile.Size = new System.Drawing.Size(86, 28);
            this.buttonWhile.TabIndex = 6;
            this.buttonWhile.Text = "while";
            this.buttonWhile.UseVisualStyleBackColor = true;
            this.buttonWhile.Click += new System.EventHandler(this.buttonWhile_Click);
            // 
            // buttonDoWhile
            // 
            this.buttonDoWhile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDoWhile.Location = new System.Drawing.Point(16, 158);
            this.buttonDoWhile.Name = "buttonDoWhile";
            this.buttonDoWhile.Size = new System.Drawing.Size(86, 28);
            this.buttonDoWhile.TabIndex = 7;
            this.buttonDoWhile.Text = "do while";
            this.buttonDoWhile.UseVisualStyleBackColor = true;
            this.buttonDoWhile.Click += new System.EventHandler(this.buttonDoWhile_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 503);
            this.Controls.Add(this.buttonDoWhile);
            this.Controls.Add(this.buttonWhile);
            this.Controls.Add(this.buttonForeach);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonFor);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.richTextBox1);
            this.Name = "mainForm";
            this.Text = "Таблица умножения";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonFor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonForeach;
        private System.Windows.Forms.Button buttonWhile;
        private System.Windows.Forms.Button buttonDoWhile;
    }
}

