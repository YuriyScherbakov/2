﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiplicationTable
{
    public static class TableDrawer
    {
        public static RichTextBox forUsing(RichTextBox rtb,int dimentions)
        {
            for ( int i = 0; i < dimentions; i++ )
            {
                if ( i != 0 )
                {
                    rtb.AppendText(i.ToString().PadRight(9));
                   
                }
                for ( int j = 0; j < dimentions; j++ )
                {
                    if ( i == 0 )
                    {
                        if ( j == 0 )
                        {
                            rtb.AppendText(" ".ToString().PadRight(10));
                            continue;
                        }
                        rtb.AppendText(j.ToString().PadRight(9));
                        continue;
                    }
                    else
                    {
                        if ( j == 0 )
                        {
                            continue;
                        }

                        if ( i == j )
                        {
                            rtb.SelectionColor = Color.Red;
                        }

                        string str = ( i * j ).ToString();
                        rtb.AppendText(str.PadRight(10 - str.Length));
                        rtb.SelectionColor = Color.Black;
                    }
                   
                }
                rtb.AppendText(Environment.NewLine);

               
            }

            return rtb;
        }

        public static RichTextBox doWhileUsing(RichTextBox rtb,int dimentions)
        {
            int i = -1;
            int j = -1;

            do
            {
                i++;
                if ( i != 0 )
                {
                    rtb.AppendText(i.ToString().PadRight(9));

                }
                do
                {
                    j++;
                    if ( i == 0 )
                    {
                        if ( j == 0 )
                        {
                            rtb.AppendText(" ".ToString().PadRight(10));
                            continue;
                        }
                        rtb.AppendText(j.ToString().PadRight(9));
                        continue;
                    }
                    else
                    {
                        if ( j == 0 )
                        {
                            continue;
                        }

                        if ( i == j )
                        {
                            rtb.SelectionColor = Color.Red;
                        }

                        string str = ( i * j ).ToString();
                        rtb.AppendText(str.PadRight(10 - str.Length));
                        rtb.SelectionColor = Color.Black;
                    }

                }
                while ( j < dimentions - 1 );
                j = -1;
                rtb.AppendText(Environment.NewLine);


            }
            while ( i < dimentions - 1 );

            return rtb;
        }

        public static RichTextBox WhileUsing(RichTextBox rtb,int dimentions)
        {
            int i = -1;
            int j = -1;

            while ( i < dimentions - 1 )
              
            {
                i++;
                if ( i != 0 )
                {
                    rtb.AppendText(i.ToString().PadRight(9));

                }
                while ( j < dimentions - 1 )
                   
                {
                    j++;
                    if ( i == 0 )
                    {
                        if ( j == 0 )
                        {
                            rtb.AppendText(" ".ToString().PadRight(10));
                            continue;
                        }
                        rtb.AppendText(j.ToString().PadRight(9));
                        continue;
                    }
                    else
                    {
                        if ( j == 0 )
                        {
                            continue;
                        }

                        if ( i == j )
                        {
                            rtb.SelectionColor = Color.Red;
                        }

                        string str = ( i * j ).ToString();
                        rtb.AppendText(str.PadRight(10 - str.Length));
                        rtb.SelectionColor = Color.Black;
                    }

                }
               
                j = -1;
                rtb.AppendText(Environment.NewLine);


            }
            

            return rtb;
        }

        public static RichTextBox ForeachUsing(RichTextBox rtb,int dimentions)
        {
            int [] i = new int [dimentions];
            int [] j = new int [dimentions];
            for ( int a = 0; a < dimentions; a++ )
            {
                i [a] = a;
                j [a] = a;

            }


            foreach ( int ia in i )
            {
                if ( ia != 0 )
                {
                    rtb.AppendText(ia.ToString().PadRight(9));

                }
                foreach ( int ja in j )
                {
                    if ( ia == 0 )
                    {
                        if ( ja == 0 )
                        {
                            rtb.AppendText(" ".ToString().PadRight(10));
                            continue;
                        }
                        rtb.AppendText(ja.ToString().PadRight(9));
                        continue;
                    }
                    else
                    {
                        if ( ja == 0 )
                        {
                            continue;
                        }

                        if ( ia == ja )
                        {
                            rtb.SelectionColor = Color.Red;
                        }

                        string str = ( ia * ja ).ToString();
                        rtb.AppendText(str.PadRight(10 - str.Length));
                        rtb.SelectionColor = Color.Black;
                    }

                }
                rtb.AppendText(Environment.NewLine);


            }

            return rtb;
        }
    }
}
