﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenericClass
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender,EventArgs e)
        {
            List<myGeneric<int>> myList = new List<myGeneric<int>>();
                        
            myGeneric<int> myG1 = new myGeneric<int>();
            myGeneric<int> myG2 = new myGeneric<int>();
            myGeneric<int> myG3 = new myGeneric<int>();
            myGeneric<int> myG4 = new myGeneric<int>();
            myGeneric<int> myG5 = new myGeneric<int>();

            myG1.myT = 1;
            myG2.myT = 2;
            myG3.myT = 3;
            myG4.myT = 4;
            myG5.myT = 5;
            

            myList.Add(myG1);
            myList.Add(myG5);
            myList.Add(myG1);
            myList.Add(myG4);
            myList.Add(myG4);
            myList.Add(myG3);
            myList.Add(myG2);

            foreach ( var item in myList)
            {
                label3.Text += item.myT;
                label3.Text += " ";
            } 

           myList.Sort();

            foreach ( var item in myList )
            {
                label4.Text += item.myT;
                label4.Text += " ";
            }


           
            
            myGeneric<double> Works = new myGeneric<double>();

           // myGeneric<string> doesntWork = new myGeneric<string>(); Не сработает (ссылочный тип)

        }


    }
    public class myGeneric<T> : IComparable <myGeneric<T>> where T : struct, IComparable<T>
    {
      
        T t;

        public T myT
        {
            set
            {
                this.t = value;
                    }
            get
            {
                return t;
            }
        }

     
        public int CompareTo(myGeneric<T> other)
        {
            return this.myT.CompareTo(other.myT);
        }
    }
}
